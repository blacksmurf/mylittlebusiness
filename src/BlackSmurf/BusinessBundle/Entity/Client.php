<?php

namespace BlackSmurf\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="client")
 */
class Client {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @var string
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @var string
     */
    protected $email;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="clients")
     * @ORM\JoinColumn(name="companySiret", referencedColumnName="siret")
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="Benefit", mappedBy="client")
     * @var Benefit
     */
    protected $benefits;

    /**
     * @ORM\OneToMany(targetEntity="BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole", mappedBy="client")
     * @var Group
     */
    protected $rights;

    /**
     * @ORM\OneToMany(targetEntity="Bill", mappedBy="client")
     * @var Bill
     */
    protected $bills;

    /**
     * @ORM\Column(type="decimal", scale=2, options={"default":0})
     * @Assert\GreaterThanOrEqual("0")
     * @var float
     */
    protected $percent;

    public function __toString() {
        return $this->getName();
    }

    ////////////////////////////////////////////////////////////////////////////
    // CODE ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->benefits = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rights = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bills = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Client
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return Client
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set company
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Company $company
     * @return Client
     */
    public function setCompany(\BlackSmurf\BusinessBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Add benefits
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Benefit $benefits
     * @return Client
     */
    public function addBenefit(\BlackSmurf\BusinessBundle\Entity\Benefit $benefits)
    {
        $this->benefits[] = $benefits;

        return $this;
    }

    /**
     * Remove benefits
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Benefit $benefits
     */
    public function removeBenefit(\BlackSmurf\BusinessBundle\Entity\Benefit $benefits)
    {
        $this->benefits->removeElement($benefits);
    }

    /**
     * Get benefits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBenefits()
    {
        return $this->benefits;
    }

    /**
     * Add rights
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $rights
     * @return Client
     */
    public function addRight(\BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $rights)
    {
        $this->rights[] = $rights;

        return $this;
    }

    /**
     * Remove rights
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $rights
     */
    public function removeRight(\BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $rights)
    {
        $this->rights->removeElement($rights);
    }

    /**
     * Get rights
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRights()
    {
        return $this->rights;
    }

    /**
     * Add bills
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Bill $bills
     * @return Client
     */
    public function addBill(\BlackSmurf\BusinessBundle\Entity\Bill $bills)
    {
        $this->bills[] = $bills;

        return $this;
    }

    /**
     * Remove bills
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Bill $bills
     */
    public function removeBill(\BlackSmurf\BusinessBundle\Entity\Bill $bills)
    {
        $this->bills->removeElement($bills);
    }

    /**
     * Get bills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBills()
    {
        return $this->bills;
    }

    /**
     * Set percent
     *
     * @param string $percent
     * @return Company
     */
    public function setPercent($percent) {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return string
     */
    public function getPercent() {
        return $this->percent;
    }
}
