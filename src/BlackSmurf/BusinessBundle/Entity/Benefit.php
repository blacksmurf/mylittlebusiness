<?php

namespace BlackSmurf\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BlackSmurf\BusinessBundle\Entity\BenefitRepository")
 * @ORM\Table(name="benefit", uniqueConstraints={@ORM\UniqueConstraint(name="IDX_ONCE_BENEFIT", columns={"clientId", "activityId", "date"})})
 */
class Benefit {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="benefits")
     * @ORM\JoinColumn(name="clientId", referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="Activity", inversedBy="benefits")
     * @ORM\JoinColumn(name="activityId", referencedColumnName="id")
     */
    protected $activity;

    /**
     * @ORM\Column(type="datetime")
     * @var datetime
     */
    protected $date;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @var float
     */
    protected $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="Bill", inversedBy="benefits")
     * @ORM\JoinColumn("billId", referencedColumnName="id")
     * @var type
     */
    protected $bill;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @var float
     */
    protected $price;

    public function __toString() {
        return $this->getClient()->getName();
    }

    ////////////////////////////////////////////////////////////////////////////
    // CODE ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Benefit
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return Benefit
     */
    public function setQuantity($quantity) {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float
     */
    public function getQuantity() {
        return $this->quantity;
    }

    /**
     * Set client
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Client $client
     * @return Benefit
     */
    public function setClient(\BlackSmurf\BusinessBundle\Entity\Client $client = null) {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Client
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * Set activity
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Activity $activity
     * @return Benefit
     */
    public function setActivity(\BlackSmurf\BusinessBundle\Entity\Activity $activity = null) {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Activity
     */
    public function getActivity() {
        return $this->activity;
    }

    /**
     * Set bill
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Bill $bill
     * @return Benefit
     */
    public function setBill(\BlackSmurf\BusinessBundle\Entity\Bill $bill = null) {
        $this->bill = $bill;

        return $this;
    }

    /**
     * Get bill
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Bill
     */
    public function getBill() {
        return $this->bill;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Benefit
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice() {
        return $this->price;
    }

}
