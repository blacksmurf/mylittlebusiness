<?php

namespace BlackSmurf\BusinessBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class CompanyRepository extends EntityRepository {

    public function findBySiret($siret) {
        return $this->createQueryBuilder('*')
                        ->where("siret like '" . $siret . "'");
    }

}
