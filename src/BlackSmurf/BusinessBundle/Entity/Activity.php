<?php

namespace BlackSmurf\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="activity")
 * @ORM\Entity(repositoryClass="BlackSmurf\BusinessBundle\Entity\ActivityRepository")
 */
class Activity {

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=5)
     * @var string
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="activities")
     * @ORM\JoinColumn(name="companySiret", referencedColumnName="siret")
     */
    protected $company;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @var float
     */
    protected $price;

    /**
     * @ORM\OneToMany(targetEntity="Benefit", mappedBy="activity")
     * @var type
     */
    protected $benefits;


    ////////////////////////////////////////////////////////////////////////////
    // MODIFIED CODE ///////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    public function getCompanySiret() {
        if ($this->company) {
            return $this->company->getSiret();
        }
    }

    public function getCompleteName() {
        return $this->getName() . " - " . $this->getDescription();
    }

    public function __toString() {
        return $this->getName();
    }

    ////////////////////////////////////////////////////////////////////////////
    // CODE ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Constructor
     */
    public function __construct() {
        $this->benefits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Activity
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Activity
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Activity
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Activity
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Set company
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Company $company
     * @return Activity
     */
    public function setCompany(\BlackSmurf\BusinessBundle\Entity\Company $company) {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Company
     */
    public function getCompany() {
        return $this->company;
    }

    /**
     * Add benefits
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Benefit $benefits
     * @return Activity
     */
    public function addBenefit(\BlackSmurf\BusinessBundle\Entity\Benefit $benefits) {
        $this->benefits[] = $benefits;

        return $this;
    }

    /**
     * Remove benefits
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Benefit $benefits
     */
    public function removeBenefit(\BlackSmurf\BusinessBundle\Entity\Benefit $benefits) {
        $this->benefits->removeElement($benefits);
    }

    /**
     * Get benefits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBenefits() {
        return $this->benefits;
    }

}
