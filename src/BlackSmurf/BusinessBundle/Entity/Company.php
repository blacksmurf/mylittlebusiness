<?php

namespace BlackSmurf\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use BlackSmurf\BusinessBundle\Entity\Charge;
use BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole;

/**
 * @ORM\Entity(repositoryClass="BlackSmurf\BusinessBundle\Entity\CompanyRepository")
 * @ORM\Table(name="company")
 */
class Company {

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=14)
     * @var string
     */
    protected $siret;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    protected $shortName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @var string
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var datetime
     */
    protected $startDateActivity;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @var string
     */
    protected $apeCode;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @var boolean
     */
    protected $eirlStatus;

    /**
     * @ORM\Column(type="decimal", scale=2, options={"default":40})
     * @var float
     */
    protected $defaultPrice;

    /**
     * @ORM\OneToMany(targetEntity="Activity", mappedBy="company")
     * @var Activity
     */
    protected $activities;

    /**
     * @ORM\OneToMany(targetEntity="Client", mappedBy="company")
     * @var Client
     */
    protected $clients;

    /**
     * @ORM\OneToMany(targetEntity="Bill", mappedBy="company")
     * @var Bill
     */
    protected $bills;


    /**
     * @ORM\Column(type="decimal", scale=2)
     * @var float
     *
     */
    protected $charges;

    /**
     * @ORM\OneToMany(targetEntity="BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole", mappedBy="company")
     * @var UserGroupRole
     */
    protected $rights;

////////////////////////////////////////////////////////////////////////////
// MODIFIED CODE ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

    public function __toString() {
        return $this->getSiret();
    }

    /**
     * Get siren
     *
     * @return string
     */
    public function getSiren() {
        return substr($this->siret, 0, 3) . " " . substr($this->siret, 3, 3) . " " . substr($this->siret, 6, 3);
    }

    /**
     * Get logoFile
     *
     * @return string
     */
    public function getLogoFile() {
        return $this->siret . ".png";
    }

    public function getUploadDir() {
        return __DIR__ . '/../../../../web/uploads';
    }

    public function isLogoFileExist() {
        $targetFile = $this->getUploadDir() . "/" . $this->getLogoFile();

        return file_exists($targetFile);
    }

    public function upload(UploadedFile $file) {
        if (is_null($file)) {
            return;
        }

        if ($this->isLogoFileExist()) {
            unlink($this->getUploadDir() . "/" . $this->getLogoFile());
        }

        $file->move($this->getUploadDir(), $this->getLogoFile());
    }

    ////////////////////////////////////////////////////////////////////////////
    // CODE ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Constructor
     */
    public function __construct() {
        $this->activities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clients = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bills = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rights = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set siret
     *
     * @param string $siret
     * @return Company
     */
    public function setSiret($siret) {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret() {
        return $this->siret;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     * @return Company
     */
    public function setShortName($shortName) {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName() {
        return $this->shortName;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Company
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Company
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return Company
     */
    public function setPhoneNumber($phoneNumber) {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber() {
        return $this->phoneNumber;
    }

    /**
     * Set startDateActivity
     *
     * @param \DateTime $startDateActivity
     * @return Company
     */
    public function setStartDateActivity($startDateActivity) {
        $this->startDateActivity = $startDateActivity;

        return $this;
    }

    /**
     * Get startDateActivity
     *
     * @return \DateTime
     */
    public function getStartDateActivity() {
        return $this->startDateActivity;
    }

    /**
     * Set apeCode
     *
     * @param string $apeCode
     * @return Company
     */
    public function setApeCode($apeCode) {
        $this->apeCode = $apeCode;

        return $this;
    }

    /**
     * Get apeCode
     *
     * @return string
     */
    public function getApeCode() {
        return $this->apeCode;
    }

    /**
     * Set eirlStatus
     *
     * @param boolean $eirlStatus
     * @return Company
     */
    public function setEirlStatus($eirlStatus) {
        $this->eirlStatus = $eirlStatus;

        return $this;
    }

    /**
     * Get eirlStatus
     *
     * @return boolean
     */
    public function getEirlStatus() {
        return $this->eirlStatus;
    }

    /**
     * Set defaultPrice
     *
     * @param string $defaultPrice
     * @return Company
     */
    public function setDefaultPrice($defaultPrice) {
        $this->defaultPrice = $defaultPrice;

        return $this;
    }

    /**
     * Get defaultPrice
     *
     * @return string
     */
    public function getDefaultPrice() {
        return $this->defaultPrice;
    }

    /**
     * Add activities
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Activity $activities
     * @return Company
     */
    public function addActivity(\BlackSmurf\BusinessBundle\Entity\Activity $activities) {
        $this->activities[] = $activities;

        return $this;
    }

    /**
     * Remove activities
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Activity $activities
     */
    public function removeActivity(\BlackSmurf\BusinessBundle\Entity\Activity $activities) {
        $this->activities->removeElement($activities);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities() {
        return $this->activities;
    }

    /**
     * Add clients
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Client $clients
     * @return Company
     */
    public function addClient(\BlackSmurf\BusinessBundle\Entity\Client $clients) {
        $this->clients[] = $clients;

        return $this;
    }

    /**
     * Remove clients
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Client $clients
     */
    public function removeClient(\BlackSmurf\BusinessBundle\Entity\Client $clients) {
        $this->clients->removeElement($clients);
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClients() {
        return $this->clients;
    }

    /**
     * Add bills
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Bill $bills
     * @return Company
     */
    public function addBill(\BlackSmurf\BusinessBundle\Entity\Bill $bills) {
        $this->bills[] = $bills;

        return $this;
    }

    /**
     * Remove bills
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Bill $bills
     */
    public function removeBill(\BlackSmurf\BusinessBundle\Entity\Bill $bills) {
        $this->bills->removeElement($bills);
    }

    /**
     * Get bills
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBills() {
        return $this->bills;
    }

    /**
     * Add rights
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $rights
     * @return Company
     */
    public function addRight(\BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $rights) {
        $this->rights[] = $rights;

        return $this;
    }

    /**
     * Remove rights
     *
     * @param \BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $rights
     */
    public function removeRight(\BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole $rights) {
        $this->rights->removeElement($rights);
    }

    /**
     * Get rights
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRights() {
        return $this->rights;
    }


    /**
     * Set charges
     *
     * @param float $charges
     */
    public function setCharges($charges) {
        $this->charges = $charges;
    }

    /**
     * Get charges
     *
     * @return float
     */
    public function getCharges() {
        return $this->charges;
    }

}
