<?php

namespace BlackSmurf\BusinessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="BlackSmurf\BusinessBundle\Entity\BillRepository")
 * @ORM\Table(name="bill")
 */
class Bill {

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=14)
     * @var string
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Benefit", mappedBy="bill")
     * @var Benefit
     */
    protected $benefits;

    /**
     * @ORM\Column(type="string", length=50)
     * @var string
     */
    protected $title;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @var float
     */
    protected $totalPrice;

    /**
     * @ORM\Column(type="decimal", scale=2, options={"default":0})
     * @var float
     */
    protected $handoff;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     * @var float
     */
    protected $charges;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     * @var boolean
     */
    protected $accepted;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $date_initial;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime
     */
    protected $date_accepted;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime
     */
    protected $declared_date;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="bills")
     * @ORM\JoinColumn("clientId", referencedColumnName="id")
     * @var Client
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="bills")
     * @ORM\JoinColumn("companySiret", referencedColumnName="siret")
     * @var Company
     */
    protected $company;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @var string
     */
    protected $payment_reference;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     * @var integer
     */
    protected $payment_mode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime
     */
    protected $payment_date;

    const NONE = 0;
    const CHECK = 1;
    const CREDIT_CARD = 2;
    const CASH = 3;
    const PAYPAL = 4;

    ////////////////////////////////////////////////////////////////////////////
    // MODIFIED CODE ///////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    public function getTotalHandOffPrice() {
        if ($this->handoff != 0) {
            return $this->getTotalPrice() * (1 - $this->handoff / 100);
        }

        return $this->getTotalPrice();
    }

    ////////////////////////////////////////////////////////////////////////////
    // CODE ////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Constructor
     */
    public function __construct() {
        $this->benefits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Bill
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     * @return Bill
     */
    public function setTotalPrice($totalPrice) {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string
     */
    public function getTotalPrice() {
        return $this->totalPrice;
    }

    /**
     * Set accepted
     *
     * @param boolean $accepted
     * @return Bill
     */
    public function setAccepted($accepted) {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * Get accepted
     *
     * @return boolean
     */
    public function getAccepted() {
        return $this->accepted;
    }

    /**
     * Set date_initial
     *
     * @param \DateTime $dateInitial
     * @return Bill
     */
    public function setDateInitial($dateInitial) {
        $this->date_initial = $dateInitial;

        return $this;
    }

    /**
     * Get date_initial
     *
     * @return \DateTime
     */
    public function getDateInitial() {
        return $this->date_initial;
    }

    /**
     * Set date_accepted
     *
     * @param \DateTime $dateAccepted
     * @return Bill
     */
    public function setDateAccepted($dateAccepted) {
        $this->date_accepted = $dateAccepted;

        return $this;
    }

    /**
     * Get date_accepted
     *
     * @return \DateTime
     */
    public function getDateAccepted() {
        return $this->date_accepted;
    }

    /**
     * Add benefits
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Benefit $benefits
     * @return Bill
     */
    public function addBenefit(\BlackSmurf\BusinessBundle\Entity\Benefit $benefits) {
        $this->benefits[] = $benefits;

        return $this;
    }

    /**
     * Remove benefits
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Benefit $benefits
     */
    public function removeBenefit(\BlackSmurf\BusinessBundle\Entity\Benefit $benefits) {
        $this->benefits->removeElement($benefits);
    }

    /**
     * Get benefits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBenefits() {
        return $this->benefits;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Bill
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set client
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Client $client
     * @return Bill
     */
    public function setClient(\BlackSmurf\BusinessBundle\Entity\Client $client = null) {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Client
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * Set company
     *
     * @param \BlackSmurf\BusinessBundle\Entity\Company $company
     * @return Bill
     */
    public function setCompany(\BlackSmurf\BusinessBundle\Entity\Company $company = null) {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \BlackSmurf\BusinessBundle\Entity\Company
     */
    public function getCompany() {
        return $this->company;
    }


    /**
     * Set charges
     *
     * @param float $charges
     * @return Bill
     */
    public function setCharges($charges) {
        $this->charges = $charges;

        return $this;
    }

    /**
     * Get charges
     *
     * @return float
     */
    public function getCharges() {
        return $this->charges;
    }

    /**
     * Set handoff
     *
     * @param string $handoff
     * @return Bill
     */
    public function setHandoff($handoff) {
        $this->handoff = $handoff;

        return $this;
    }

    /**
     * Get handoff
     *
     * @return string
     */
    public function getHandoff() {
        return $this->handoff;
    }

    /**
     * Set payment_reference
     *
     * @param string $paymentReference
     * @return Bill
     */
    public function setPaymentReference($paymentReference) {
        $this->payment_reference = $paymentReference;

        return $this;
    }

    /**
     * Get payment_reference
     *
     * @return string
     */
    public function getPaymentReference() {
        return $this->payment_reference;
    }

    /**
     * Set payment_mode
     *
     * @param integer $paymentMode
     * @return Bill
     */
    public function setPaymentMode($paymentMode) {
        $this->payment_mode = $paymentMode;

        return $this;
    }

    /**
     * Get payment_mode
     *
     * @return integer
     */
    public function getPaymentMode() {
        return $this->payment_mode;
    }

    /**
     * Set payment_date
     *
     * @param \DateTime $paymentDate
     * @return Bill
     */
    public function setPaymentDate($paymentDate) {
        $this->payment_date = $paymentDate;

        return $this;
    }

    /**
     * Get payment_date
     *
     * @return \DateTime
     */
    public function getPaymentDate() {
        return $this->payment_date;
    }


    /**
     * Set declared_date
     *
     * @param \DateTime $declaredDate
     * @return Bill
     */
    public function setDeclaredDate($declaredDate) {
        $this->declared_date = $declaredDate;

        return $this;
    }

    /**
     * Get declared_date
     *
     * @return \DateTime
     */
    public function getDeclaredDate() {
        return $this->declared_date;
    }


}
