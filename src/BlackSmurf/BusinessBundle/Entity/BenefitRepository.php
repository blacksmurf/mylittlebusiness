<?php

namespace BlackSmurf\BusinessBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class BenefitRepository extends EntityRepository {

    public function findByCompany(Company $company) {
        return $this->getEntityManager()
                        ->createQuery("SELECT b FROM BlackSmurfBusinessBundle:Benefit b, BlackSmurfBusinessBundle:Client c WHERE b.client = c AND c.company = '" . $company . "'")
                        ->getResult();
    }

    public function getTotalBenefits(Company $company) {
        return $this->getEntityManager()
                        ->createQuery("SELECT SUM(b.price*b.quantity) as total FROM BlackSmurfBusinessBundle:Benefit b, BlackSmurfBusinessBundle:Client c WHERE b.client = c AND c.company = '" . $company . "'")
                        ->getResult();
    }

    public function findBenefitsWithBill(Company $company) {
        return $this->getEntityManager()
                        ->createQuery("SELECT b FROM BlackSmurfBusinessBundle:Benefit b, BlackSmurfBusinessBundle:Client c WHERE b.bill is not null AND b.client = c AND c.company = '" . $company . "' GROUP BY b.bill")
                        ->getResult();
    }

    public function getAllBenefitsAcceptedBillFromClient($year, Client $client) {
        return $this->getEntityManager()
                        ->createQuery("SELECT benefit FROM BlackSmurfBusinessBundle:Benefit benefit, BlackSmurfBusinessBundle:Bill bill WHERE benefit.client = " . $client->getId() . " AND benefit.bill = bill AND bill.accepted = TRUE AND bill.date_accepted between '" . $year . "-01-01' AND '" . $year . "-12-31' ")
                        ->getResult();
    }

    public function getAllBenefitsStandingBillFromClient($year, Client $client) {
        return $this->getEntityManager()
                        ->createQuery("SELECT benefit FROM BlackSmurfBusinessBundle:Benefit benefit, BlackSmurfBusinessBundle:Bill bill WHERE benefit.client = " . $client->getId() . " AND benefit.bill = bill AND bill.accepted = FALSE AND bill.date_initial between '" . $year . "-01-01' AND '" . $year . "-12-31' ")
                        ->getResult();
    }

    public function getAllBenefitsUnBillFromClient($year, Client $client) {
        return $this->getEntityManager()
                        ->createQuery("SELECT benefit FROM BlackSmurfBusinessBundle:Benefit benefit  WHERE benefit.client = " . $client->getId() . " AND benefit.bill is null AND benefit.date between '" . $year . "-01-01' AND '" . $year . "-12-31' ")
                        ->getResult();
    }

    public function getAllBenefitsAcceptedBillFromCompany($year, Company $company) {
        return $this->getEntityManager()
                        ->createQuery("SELECT benefit FROM BlackSmurfBusinessBundle:Benefit benefit, BlackSmurfBusinessBundle:Bill bill, BlackSmurfBusinessBundle:Client client WHERE benefit.client = client AND client.company = " . $company . " AND benefit.bill = bill AND bill.accepted = TRUE AND bill.date_accepted between '" . $year . "-01-01' AND '" . $year . "-12-31' ")
                        ->getResult();
    }

    public function getAllBenefitsUnBillFromCompany($year, Company $company) {
        return $this->getEntityManager()
                        ->createQuery("SELECT benefit FROM BlackSmurfBusinessBundle:Benefit benefit, BlackSmurfBusinessBundle:Client client WHERE benefit.client = client AND client.company = " . $company . " AND benefit.bill is null AND benefit.date between '" . $year . "-01-01' AND '" . $year . "-12-31' ")
                        ->getResult();
    }

    public function getAllBenefitsStandingBillFromCompany($year, Company $company) {
        return $this->getEntityManager()
                        ->createQuery("SELECT benefit FROM BlackSmurfBusinessBundle:Benefit benefit, BlackSmurfBusinessBundle:Bill bill, BlackSmurfBusinessBundle:Client client WHERE benefit.client = client AND client.company = " . $company . " AND benefit.bill = bill AND bill.accepted = FALSE AND bill.date_initial between '" . $year . "-01-01' AND '" . $year . "-12-31' ")
                        ->getResult();
    }

    public function getAllYearsOfBenefitsFromCompany(Company $company) {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('year', 'year');
        return $this->getEntityManager()
                        ->createNativeQuery("SELECT DISTINCT(EXTRACT(YEAR FROM date)) AS year FROM benefit, client WHERE benefit.clientId = client.id AND client.companySiret like '" . $company->getSiret() . "' ORDER BY year", $rsm)
                        ->getResult();

  }

    public function getAllYearsOfBenefitsFromClient(Client $client) {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('year', 'year');
        return $this->getEntityManager()
            ->createNativeQuery("SELECT DISTINCT(EXTRACT(YEAR FROM date)) AS year FROM benefit, client WHERE benefit.clientId = " . $client->getId() . " ORDER BY year", $rsm)
            ->getResult();

    }

}
