<?php

namespace BlackSmurf\BusinessBundle\Controller;

// SYMFONY //
use Symfony\Component\HttpFoundation\Request;
// ANNOTATIONS //
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
// SYMFONY2COREBUNDLE //
use BlackSmurf\Symfony2CoreBundle\Controller\EntityController;
// PROJECT //
use BlackSmurf\BusinessBundle\Entity\Activity;
use BlackSmurf\BusinessBundle\Form\ActivityType;
use BlackSmurf\BusinessBundle\Entity\Charge;

/**
 * Activity controller.
 *
 * @Route("/activity")
 */
class ActivityController extends EntityController {

    ////////////////////////////////////////////////////////////////////////////
    // MUST BE DEFINED /////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    protected $routes = array(
        "index" => "activity_index",
        "new" => "activity_new",
        "create" => "activity_create",
        "edit" => "activity_edit",
        "update" => "activity_update",
        "delete" => "activity_delete"
    );

    /**
     * Return string object URI
     *
     * @return string
     */
    protected function getEntityURI() {
        return "BlackSmurfBusinessBundle:Activity";
    }

    /**
     * Return new object
     *
     * @return Activity
     */
    protected function getNewEntity() {
        return new Activity();
    }

    /**
     * Return Form's object
     *
     * @return ActivityType
     */
    protected function getNewEntityFormType() {
        return new ActivityType();
    }

    ////////////////////////////////////////////////////////////////////////////
    // LISTING /////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Lists all Activity entities.
     *
     * @Route("/", name="activity_index")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_GEST")
     */
    public function indexAction() {

        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        $entities = NULL;

        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
        } else {

            // select all activities from user's profile (company)
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository($this->getEntityURI())
                    ->findBy(array("company" => $userGroupRole->getCompany()));
        }

        $charges = $userGroupRole->getCompany()->getCharges() / 100;

        // render the view
        return $this->buildParameters(array(
                    'entities' => $entities,
                    'charges' => $charges
        ));
    }

    ////////////////////////////////////////////////////////////////////////////
    // NEW/CREATE //////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Displays a form to create a new Activity entity.
     *
     * @Route("/new", name="activity_new")
     * @Method("GET")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function newAction() {

        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
            return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
        }

        // init the activity object
        $entity = $this->getNewEntity();
        $entity->setPrice($userGroupRole->getCompany()->getDefaultPrice());

        // create the object's form
        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST");

        // render the view
        return $this->buildParameters(array(
                    'page_title' => 'Ajouter une nouvelle activité',
                    'entity' => $entity,
                    'entityForm' => $form->createView(),
        ));
    }

    /**
     * Creates a new Activity entity.
     *
     * @Route("/", name="activity_create")
     * @Method("POST")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function createAction(Request $request) {

        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
            return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
        }

        // init the activity object
        $entity = $this->getNewEntity();

        // create the form and map it with object
        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST");

        // mapping form with request
        $form->handleRequest($request);

        // if form is not valid, show the form and exit
        if ($form->isValid()) {
            try {
                // finalize object and persist it to db
                $newId = $this->getNewId($entity);
                $entity->setId($newId);
                $name = $entity->getName();
                $entity->setName(strtoupper($name));
                $entity->setCompany($userGroupRole->getCompany());

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                // exit
                $this->successMessage("L'activité '$name' a bien été créé");
                return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
            } catch (\Exception $e) {

                $this->errorMessage("L'activité ne peut être ajouté");
            }
        }

        // exit
        return $this->buildParameters(array(
                    'page_title' => 'Ajouter une nouvelle activité',
                    'entity' => $entity,
                    'entityForm' => $form->createView(),
        ));
    }

    ////////////////////////////////////////////////////////////////////////////
    // EDIT/UPDATE /////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Displays a form to edit an existing Activity entity.
     *
     * @Route("/{id}/edit", name="activity_edit")
     * @Method("GET")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function editAction($id) {

        // user can edit this object? => CSRF control
        try {
            $this->checkCSRFProtection();
        } catch (\Exception $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        // get object from db
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->getEntityURI())
                ->findOneBy(array("id" => $id));

        if (!is_null($entity)) {
            // create the form and map it with object
            $editForm = $this->createMyForm($entity, $this->generateUrl($this->getRoute('update'), array('id' => $entity->getId())), "PUT");

            // pass forms, object to view and exit
            return $this->buildParameters(array(
                        'page_title' => "Modification de l'activité '" . $entity->getName() . "'",
                        'entity' => $entity,
                        'entityForm' => $editForm->createView(),
            ));
        }

        // exit
        $this->errorMessage("Impossible de trouver l'activité !");
        return $this->redirect($this->generateUrl($this->getRoute('index')));
    }

    /**
     * Edits an existing Activity entity.
     *
     * @Route("/{id}", name="activity_update")
     * @Method("PUT")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function updateAction(Request $request, $id) {

        // get original object from db
        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository($this->getEntityURI())
                ->findOneBy(array("id" => $id));

        if (is_null($entity)) {
            $this->errorMessage("Impossible de trouver l'activité !");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        // create the form and map it with object
        $editForm = $this->createMyForm($entity, $this->generateUrl($this->getRoute('update'), array('id' => $entity->getId())), "PUT");

        // mapping form with request
        $editForm->handleRequest($request);

        // any errors?
        if ($editForm->isValid()) {
            try {
                // finalize object and persist it to db
                //$newId = $this->getNewId($entity);
                //$entity->setId($newId);
                $name = $entity->getName();
                $entity->setName(strtoupper($name));
                $em->flush();
                $this->successMessage("L'activité '$name' a bien été modifié.");

                // exit
                return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $id)));
            } catch (\Exception $e) {

                $this->errorMessage("L'activité '$name' ne peut pas être modifié !");
            }
        }

        // exit
        return $this->buildParameters(array(
                    'page_title' => "Modification de l'activité '" . $entity->getName() . "'",
                    'entity' => $entity,
                    'entityForm' => $editForm->createView(),
        ));
    }

    ////////////////////////////////////////////////////////////////////////////
    // DELETE //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Deletes a Activity entity.
     *
     * @Route("/{id}", defaults={"id" = "0"}, name="activity_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_GEST")
     */
    public function deleteAction(Request $request, $id) {
        return $this->deleteMyEntityFromId($request, $id);
    }

    ////////////////////////////////////////////////////////////////////////////
    // OTHER ///////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    private function getNewId(Activity $entity) {
        preg_match_all('/(?<=\s|^)[a-z]/i', $entity->getName(), $matches);
        $result = strtoupper(implode('', $matches[0]));

        $em = $this->getDoctrine()->getManager();
/*
        $query = $em->createQuery(
            "SELECT p.id
                FROM BlackSmurfBusinessBundle:Activity p
                WHERE p.id like '" . $result . "%'
                AND p.name not like '" . $entity->getName() . "'
                ORDER by p.id DESC"
        );
/*/
        $query = $em->createQuery(
                "SELECT p.id
                FROM BlackSmurfBusinessBundle:Activity p
                WHERE p.id like '" . $result . "%'
                ORDER by p.id DESC"
        );
//*/
        $last = $query->getResult();
        if (count($last) > 0) {
            $lastId = $last[0]["id"];
            if (!empty($lastId)) {
                $number = preg_split('/^[a-zA-Z]*/', $lastId);
                $number = $number[1] + 1;
                if ($number < 10) {
                    $number = "00" . $number;
                } else if ($number < 100) {
                    $number = "0" . $number;
                }

                return $result . $number;
            }
        }

        return $result . "001";
    }

}
