<?php

namespace BlackSmurf\BusinessBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
// ANNOTATIONS //
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
// SYMFONY2COREBUNDLE //
use BlackSmurf\Symfony2CoreBundle\Controller\EntityController;
use BlackSmurf\BusinessBundle\Entity\Charge;

/**
 * Client controller.
 *
 * @Route("/")
 */
class DefaultController extends EntityController {

    ////////////////////////////////////////////////////////////////////////////
    // MUST BE DEFINED /////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    protected $routes = array(
        "index" => "home_index",
    );

    /**
     * Return string object URI
     *
     * @return string
     */
    protected function getEntityURI() {

    }

    /**
     * Return new object
     *
     * @return Activity
     */
    protected function getNewEntity() {

    }

    /**
     * Return Form's object
     *
     * @return ActivityType
     */
    protected function getNewEntityFormType($new = true) {

    }

    /**
     *
     *
     * @param $year Year reference
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function homeAction($year) {
        $user = $this->get('security.context');
        $em = $this->getDoctrine()->getManager();

        // ROLE_CLIENT
        if ($user->isGranted('ROLE_CLIENT')) {
            // if user haven't a company
            $userGroupRole = $this->getMyUserGroupRole();

            $benefitsBilledAmount = 0;
            $benefitsBilledQuantity = 0;
            $benefitsQuoteAmount = 0;
            $benefitsQuoteQuantity = 0;
            $benefitsUnBilledAmount = 0;
            $benefitsUnBilledQuantity = 0;
            $years = array();
            $resultYears = array();

            $years = $em->getRepository('BlackSmurfBusinessBundle:Benefit')
                ->getAllYearsOfBenefitsFromClient($userGroupRole->getClient());

            foreach ($years as $resultYear) {
                $resultYears[] = $resultYear["year"];
            }


            if (is_null($userGroupRole->getClient())) {
                $this->errorMessage("Vous n'avez aucune référence de client dans votre profil !");
            }

            $benefitsAccepted = $em->getRepository('BlackSmurfBusinessBundle:Benefit')
                ->getAllBenefitsAcceptedBillFromClient($year, $userGroupRole->getClient());


            $benefitsStanding = $em->getRepository('BlackSmurfBusinessBundle:Benefit')
                ->getAllBenefitsStandingBillFromClient($year, $userGroupRole->getClient());


            $benefitsUnBilled = $em->getRepository('BlackSmurfBusinessBundle:Benefit')
                ->getAllBenefitsUnBillFromClient($year, $userGroupRole->getClient());

            foreach ($benefitsAccepted as $benefit) {
                if ($benefit->getBill()->getHandOff() != 0) {
                    $benefitsBilledAmount += ($benefit->getPrice() * $benefit->getQuantity()) * (1 - $benefit->getBill()->getHandOff() / 100);
                } else {
                    $benefitsBilledAmount += $benefit->getPrice() * $benefit->getQuantity();
                }
                $benefitsBilledQuantity += $benefit->getQuantity();
            }

            foreach ($benefitsStanding as $benefit) {
                $benefitsQuoteAmount += $benefit->getPrice() * $benefit->getQuantity();
                $benefitsQuoteQuantity += $benefit->getQuantity();
            }

            foreach ($benefitsUnBilled as $benefit) {
                $benefitsUnBilledAmount += $benefit->getPrice() * $benefit->getQuantity();
                $benefitsUnBilledQuantity += $benefit->getQuantity();
            }

            // render the view
            return $this->render('BlackSmurfBusinessBundle:Default:index_client.html.twig', $this->buildParameters(array(
                'benefitsBilledAmount' => $benefitsBilledAmount,
                'benefitsBilledQuantity' => $benefitsBilledQuantity,
                'benefitsQuoteAmount' => $benefitsQuoteAmount,
                'benefitsQuoteQuantity' => $benefitsQuoteQuantity,
                'benefitsUnBilledAmount' => $benefitsUnBilledAmount,
                'benefitsUnBilledQuantity' => $benefitsUnBilledQuantity,
                'year' => $year,
                'years' => $resultYears
            )));
        } else if ($user->isGranted('ROLE_GEST')) {
            // if user haven't a company
            $userGroupRole = $this->getMyUserGroupRole();

            if (is_null($userGroupRole->getCompany())) {
                $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
            }

            $benefitsBilledAmount = 0;
            $benefitsBilledQuantity = 0;
            $benefitsQuoteAmount = 0;
            $benefitsQuoteQuantity = 0;
            $benefitsUnBilledAmount = 0;
            $benefitsUnBilledQuantity = 0;
            $years = array();
            $resultYears = array();

            $years = $em->getRepository('BlackSmurfBusinessBundle:Benefit')
                ->getAllYearsOfBenefitsFromCompany($userGroupRole->getCompany());

            foreach ($years as $resultYear) {
                $resultYears[] = $resultYear["year"];
            }

            $benefitsAccepted = $em->getRepository('BlackSmurfBusinessBundle:Benefit')
                ->getAllBenefitsAcceptedBillFromCompany($year, $userGroupRole->getCompany());

            $benefitsStanding = $em->getRepository('BlackSmurfBusinessBundle:Benefit')
                ->getAllBenefitsStandingBillFromCompany($year, $userGroupRole->getCompany());

            $benefitsUnBilled = $em->getRepository('BlackSmurfBusinessBundle:Benefit')
                ->getAllBenefitsUnBillFromCompany($year, $userGroupRole->getCompany());

            foreach ($benefitsAccepted as $benefit) {
                if ($benefit->getBill()->getHandOff() != 0) {
                    $benefitsBilledAmount += ($benefit->getPrice() * $benefit->getQuantity()) * (1 - $benefit->getBill()->getHandOff() / 100);
                } else {
                    $benefitsBilledAmount += $benefit->getPrice() * $benefit->getQuantity();
                }
                $benefitsBilledQuantity += $benefit->getQuantity();
            }

            foreach ($benefitsStanding as $benefit) {
                $benefitsQuoteAmount += $benefit->getPrice() * $benefit->getQuantity();
                $benefitsQuoteQuantity += $benefit->getQuantity();
            }

            foreach ($benefitsUnBilled as $benefit) {
                $benefitsUnBilledAmount += $benefit->getPrice() * $benefit->getQuantity();
                $benefitsUnBilledQuantity += $benefit->getQuantity();
            }


            $charges = $userGroupRole->getCompany()->getCharges();

            // render the view
            return $this->render('BlackSmurfBusinessBundle:Default:index_gest.html.twig', $this->buildParameters(array(
                'ca' => $benefitsBilledAmount,
                'charges' => $charges*$benefitsBilledAmount/100,
                'benefitsBilledAmount' => $benefitsBilledAmount,
                'benefitsBilledQuantity' => $benefitsBilledQuantity,
                'benefitsQuoteAmount' => $benefitsQuoteAmount,
                'benefitsQuoteQuantity' => $benefitsQuoteQuantity,
                'benefitsUnBilledAmount' => $benefitsUnBilledAmount,
                'benefitsUnBilledQuantity' => $benefitsUnBilledQuantity,
                'year' => $year,
                'years' => $resultYears
            )));
        }

        return $this->render('BlackSmurfBusinessBundle:Default:home_no_login.html.twig');
    }

    ////////////////////////////////////////////////////////////////////////////
    // LISTING /////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Home
     *
     * @Route("/", name="home_index")
     * @Method("GET")
     * @Template()
     * @Secure(roles="IS_AUTHENTICATED_ANONYMOUSLY, ROLE_GEST, ROLE_CLIENT")
     */
    public function indexAction() {
        return $this->homeAction(date('Y'));
    }

    /**
     * Home
     *
     * @Route("/{year}/history", name="home_history")
     * @Method("GET")
     * @Template()
     * @Secure(roles="IS_AUTHENTICATED_ANONYMOUSLY, ROLE_GEST, ROLE_CLIENT")
     */
    public function historyAction(Request $request, $year) {
        return $this->homeAction($year);
    }

}
