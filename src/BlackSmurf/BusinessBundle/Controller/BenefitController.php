<?php

namespace BlackSmurf\BusinessBundle\Controller;

// SYMFONY //
use Symfony\Component\HttpFoundation\Request;
// ANNOTATIONS //
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
// SYMFONY2COREBUNDLE //
use BlackSmurf\Symfony2CoreBundle\Controller\EntityController;
// PROJECT //
use BlackSmurf\BusinessBundle\Entity\Benefit;
use BlackSmurf\BusinessBundle\Form\BenefitType;

/**
 * Benfit controller.
 *
 * @Route("/benefit")
 */
class BenefitController extends EntityController {

    ////////////////////////////////////////////////////////////////////////////
    // MUST BE DEFINED /////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    protected $routes = array(
        "index" => "benefit_index",
        "new" => "benefit_new",
        "create" => "benefit_create",
        "edit" => "benefit_edit",
        "update" => "benefit_update",
        "delete" => "benefit_delete"
    );

    /**
     * Return string object URI
     *
     * @return string
     */
    protected function getEntityURI() {
        return "BlackSmurfBusinessBundle:Benefit";
    }

    /**
     * Return new object
     *
     * @return Activity
     */
    protected function getNewEntity() {
        return new Benefit();
    }

    /**
     * Return Form's object
     *
     * @return ActivityType
     */
    protected function getNewEntityFormType() {
        return new BenefitType($this->getMyUserGroupRole()->getCompany());
    }

    ////////////////////////////////////////////////////////////////////////////
    // LISTING /////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Lists all Benefit entities.
     *
     * @Route("/", name="benefit_index")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_GEST")
     */
    public function indexAction() {
        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        $entities = NULL;

        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
        } else {

            // select all activities from user's profile (company)
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository($this->getEntityURI())
                    ->findByCompany($userGroupRole->getCompany());
        }

        // render the view
        return $this->buildParameters(array(
                    'entities' => $entities,
        ));
    }

    ////////////////////////////////////////////////////////////////////////////
    // NEW/CREATE //////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Displays a form to create a new Benefit entity.
     *
     * @Route("/new", name="benefit_new")
     * @Method("GET")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function newAction() {
        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
            return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
        }

        // init the activity object
        $entity = $this->getNewEntity();

        // create the object's form
        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST");

        return $this->buildParameters(array(
                    'page_title' => 'Ajouter une nouvelle prestation',
                    'entity' => $entity,
                    'entityForm' => $form->createView(),
        ));
    }

    /**
     * Creates a new Benefit entity.
     *
     * @Route("/", name="benefit_create")
     * @Method("POST")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function createAction(Request $request) {
        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
            return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
        }

        // init the activity object
        $entity = $this->getNewEntity();

        // create the form and map it with object
        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST");

        // mapping form with request
        $form->handleRequest($request);

        // if form is not valid, show the form and exit
        if ($form->isValid()) {
            try {
                // finalize object and persist it to db
                $em = $this->getDoctrine()->getManager();
                $entity->setprice($entity->getActivity()->getPrice()*(1+$entity->getClient()->getPercent()/100));
                $em->persist($entity);
                $em->flush();
                $this->successMessage("La prestation a bien été ajouté.");

                return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
            } catch (\Exception $e) {

                $this->errorMessage("La prestation ne peut être ajouté.");
            }
        }

        return $this->buildParameters(array(
                    'page_title' => 'Ajouter une nouvelle prestation',
                    'entity' => $entity,
                    'entityForm' => $form->createView(),
        ));
    }

    ////////////////////////////////////////////////////////////////////////////
    // DELETE //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes a Benefit entity.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, defaults={"id" = "0"}, name="benefit_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_GEST")
     */
    public function deleteAction(Request $request, $id) {
        return $this->deleteMyEntityFromId($request, $id);
    }

}
