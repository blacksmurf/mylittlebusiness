<?php

namespace BlackSmurf\BusinessBundle\Controller;

use \DateTime;
use \DateTimeZone;
// SYMFONY //
use BlackSmurf\BusinessBundle\Form\BillPaymentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
// ANNOTATIONS //
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
// SYMFONY2COREBUNDLE //
use BlackSmurf\Symfony2CoreBundle\Controller\EntityController;
use BlackSmurf\BusinessBundle\Entity\Bill;
use BlackSmurf\BusinessBundle\Form\BillType;

/**
 * Bill controller.
 *
 * @Route("/bill")
 */
class BillController extends EntityController {

    ////////////////////////////////////////////////////////////////////////////
    // MUST BE DEFINED /////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    protected $routes = array(
        "index" => "bill_index",
        "new" => "bill_new",
        "create" => "bill_create",
        "edit" => "bill_edit",
        "update" => "bill_update",
        "delete" => "bill_delete"
    );

    /**
     * Return string object URI
     *
     * @return string
     */
    protected function getEntityURI() {
        return "BlackSmurfBusinessBundle:Bill";
    }

    /**
     * Return new object
     *
     * @return Activity
     */
    protected function getNewEntity() {
        return new Bill();
    }

    /**
     * Return Form's object
     *
     * @return ActivityType
     */
    protected function getNewEntityFormType($new = true, $id = null) {
        return new BillType($this->getMyUserGroupRole()->getCompany(), $new, $id);
    }

    ////////////////////////////////////////////////////////////////////////////
    // LISTING /////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Lists all Bill entities.
     *
     * @Route("/", name="bill_index")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_GEST, ROLE_CLIENT")
     */
    public function indexAction() {
        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context');
        $twig = '';
        $entities = NULL;
        if ($user->isGranted('ROLE_GEST')) {
            if (is_null($userGroupRole->getCompany())) {
                $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
            } else {
                $entities = $em->getRepository("BlackSmurfBusinessBundle:Bill")
                        ->findBy(array("company" => $userGroupRole->getCompany()));
            }

            $twig = 'BlackSmurfBusinessBundle:Bill:index_gest.html.twig';
        } else if ($user->isGranted('ROLE_CLIENT')) {
            if (is_null($userGroupRole->getClient())) {
                $this->errorMessage("Vous n'avez aucune référence client dans votre profil !");
            } else {
                $entities = $em->getRepository("BlackSmurfBusinessBundle:Bill")
                        ->findBy(array("client" => $userGroupRole->getClient()));
            }

            $twig = 'BlackSmurfBusinessBundle:Bill:index_client.html.twig';
        }

        return $this->render($twig, $this->buildParameters(array(
                            'entities' => $entities
                        ))
        );
    }

    ////////////////////////////////////////////////////////////////////////////
    // NEW/CREATE //////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Displays a form to create a new Bill entity.
     *
     * @Route("/new", name="bill_new")
     * @Method("GET")
     * @Template("BlackSmurfBusinessBundle:Bill:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function newAction() {
        $entity = $this->getNewEntity();

        $entity->setHandoff(0);
        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST", true);
        $options = $form->get('benefits')->getConfig()->getOptions();
        $choices = $options['choice_list']->getChoices();

        return $this->buildParameters(array(
                    'page_title' => 'Créer une facture',
                    'entity' => $entity,
                    'choices' => $choices,
                    'entityForm' => $form->createView(),
        ));
    }

    /**
     * Creates a new Bill entity.
     *
     * @Route("/", name="bill_create")
     * @Method("POST")
     * @Template("BlackSmurfBusinessBundle:Bill:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function createAction(Request $request) {
        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
            return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
        }

        // init the activity object
        $entity = $this->getNewEntity();

        // create the form and map it with object
        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST");

        // mapping form with request
        $form->handleRequest($request);


        // if form is not valid, show the form and exit
        if ($form->isValid()) {

            try {
                // finalize object and persist it to db
                $date = date('Ymd');
                $dateFormat = DateTime::createFromFormat('Ymd', date('Ymd'), new DateTimeZone('UTC'));
                $entity->setDateInitial($dateFormat);

                if ($entity->getAccepted()) {
                    $entity->setDateAccepted($dateFormat);
                    $entity->setCharges($userGroupRole->getCompany()->getCharges());
                }

                // new ID
                $entity->setId($this->getNewId($date, $entity));

                $entity->setPaymentMode(Bill::NONE);

                $entity->setCompany($userGroupRole->getCompany());

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);

                $totalPrice = 0;

                foreach ($entity->getBenefits() as $benefit) {
                    $totalPrice += $benefit->getQuantity() * $benefit->getPrice();
                    $benefit->setBill($entity);
                    $em->persist($benefit);
                }

                $entity->setTotalPrice($totalPrice);
                $em->flush();

                $this->successMessage("La facture a bien été ajouté.");
                
                return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
            } catch (\Exception $e) {
                $this->errorMessage("La facture ne peut être créée.");
            }
        }

        // exit
        $options = $form->get('benefits')->getConfig()->getOptions();
        $choices = $options['choice_list']->getChoices();

        return $this->buildParameters(array(
                    'page_title' => 'Créer une facture',
                    'entity' => $entity,
                    'choices' => $choices,
                    'entityForm' => $form->createView(),
        ));
    }

////////////////////////////////////////////////////////////////////////////
// EDIT/UPDATE /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
    /**
     * Displays a form to edit an existing Bill entity.
     *
     * @Route("/{id}/edit", name="bill_edit")
     * @Method("GET")
     * @Template("BlackSmurfBusinessBundle:Bill:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function editAction(Request $request, $id) {
        // user can edit this object? => CSRF control
        try {
            $this->checkCSRFProtection();
        } catch (\Exception $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        // get object from db
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->getEntityURI())
                ->findOneBy(array("id" => $id));

        if (is_null($entity) || $entity->getCompany() != $this->getMyUserGroupRole()->getCompany()) {
            $this->errorMessage("Impossible de trouver la facture !");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        $editForm = $this->createForm($this->getNewEntityFormType(false, $entity->getId()), $entity, array(
            'action' => $this->generateUrl($this->getRoute('update'), array('id' => $entity->getId())),
            'method' => "PUT",
        ));
        $editForm->add('submit', 'submit', array('label' => 'Valider'));

        $options = $editForm->get('benefits')->getConfig()->getOptions();
        $choices = $options['choice_list']->getChoices();

        // exit
        return $this->buildParameters(array(
                    'page_title' => "Modification de la facture '" . $entity->getTitle() . "'",
                    'entity' => $entity,
                    'choices' => $choices,
                    'entityForm' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Bill entity.
     *
     * @Route("/{id}", name="bill_update")
     * @Method("PUT")
     * @Template("BlackSmurfBusinessBundle:Bill:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function updateAction(Request $request, $id) {

        // get original object from db
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->getEntityURI())
                ->findOneBy(array("id" => $id));

        if (is_null($entity) || $entity->getCompany() != $this->getMyUserGroupRole()->getCompany()) {
            $this->errorMessage("Impossible de trouver la facture !");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        // create the form and map it with object
        $editForm = $this->createForm($this->getNewEntityFormType(false, $entity->getId()), $entity, array(
            'action' => $this->generateUrl($this->getRoute('update'), array('id' => $entity->getId())),
            'method' => "PUT",
        ));
        $editForm->add('submit', 'submit', array('label' => 'Valider'));

        // mapping form with request
        $editForm->handleRequest($request);

        $options = $editForm->get('benefits')->getConfig()->getOptions();
        $choices = $options['choice_list']->getChoices();

        // any errors?
        if ($editForm->isValid()) {
            try {
                // finalize object and persist it to db
                $benefits = $em->getRepository("BlackSmurfBusinessBundle:Benefit")->findBy(array("bill" => $entity));
                foreach ($benefits as $benefit) {
                    $benefit->setBill(null);
                    $em->persist($benefit);
                }

                $totalPrice = 0;
                foreach ($entity->getBenefits() as $benefit) {
                    $totalPrice += $benefit->getQuantity() * $benefit->getPrice();
                    $benefit->setBill($entity);
                    $em->persist($benefit);
                }

                $entity->setTotalPrice($totalPrice);
                if ($entity->getAccepted()) {
                    $date = date('Ymd');
                    $entity->setDateAccepted(DateTime::createFromFormat('Ymd', $date, new DateTimeZone('UTC')));
                    $entity->setCharges($entity->getCompany()->getCharges());
                }

                $em->flush();
                $this->successMessage("La facture a bien été modifié.");
                return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $id)));
            } catch (\Exception $e) {
                $this->errorMessage("Le facture ne peut pas être modifié.");
            }
        }

        return $this->buildParameters(array(
                    'page_title' => "Modification de la facture '" . $entity->getTitle() . "'",
                    'entity' => $entity,
                    'choices' => $choices,
                    'entityForm' => $editForm->createView(),
        ));
    }

////////////////////////////////////////////////////////////////////////////
// DELETE //////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes a Bill entity.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, defaults={"id" = "0"}, name="bill_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_GEST")
     */
    public function deleteAction(Request $request, $id) {
        try {
            // user can delete this object? => CSRF control
            $this->checkCSRFProtection();

            // get original object from db
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($this->getEntityURI())
                    ->findOneBy(array("id" => $id));


            if (is_null($entity) || $entity->getCompany() != $this->getMyUserGroupRole()->getCompany()) {
                $this->errorMessage("Impossible de trouver la facture !");
                return $this->redirect($this->generateUrl($this->getRoute('index')));
            }

            if (!is_null($entity)) {
                // finalize object and persist it to db
                foreach ($entity->getBenefits() as $benefit) {
                    $benefit->setBill(NULL);
                }

                $em->remove($entity);
                $em->flush();
                $this->successMessage("La facture a bien été supprimé.");
            } else {
                $this->errorMessage("Impossible de trouver l'objet !");
            }
        } catch (\Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
        } catch (\Exception $e) {
            $this->errorMessage("La facture ne peut pas être supprimé pour la raison suivante : " . $e->getMessage());
        }

        // exit
        return $this->redirect($this->generateUrl($this->getRoute('index')));
    }

    /**
     * Displays a form to print an existing Bill entity.
     *
     * @Route("/{id}/print/{forceQuote}", name="bill_print", defaults={"forceQuote" = false})
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_GEST, ROLE_CLIENT")
     */
    public function downloadAction(Request $request, $id, $forceQuote) {
        try {
            // user can delete this object? => CSRF control
            $this->checkCSRFProtection();

            // get original object from db
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($this->getEntityURI())
                    ->findOneBy(array('id' => $id));

            $objet = !is_null($this->getMyUserGroupRole()->getCompany()) ? $this->getMyUserGroupRole()->getCompany() : $this->getMyUserGroupRole()->getClient();
            if (is_null($entity) || ($objet != $entity->getCompany() && $objet != $entity->getClient())) {
                $this->errorMessage("Impossible de trouver la facture !");
                return $this->redirect($this->generateUrl($this->getRoute('index')));
            }

            $benefits = array();
            foreach ($entity->getBenefits() as $benefit) {
                $i = -1;
                foreach ($benefits as $key => $value) {
                    if ($value->getActivity()->getId() == $benefit->getActivity()->getId() && $value->getPrice() == $benefit->getPrice()) {
                        $i = $key;
                        break;
                    }
                }
                if ($i != -1) {
                    $benefits[$i]->setQuantity($benefits[$i]->getQuantity() + $benefit->getQuantity());
                } else {
                    $benefits[] = $benefit;
                }
            }

            if (!$forceQuote && !$entity->getAccepted()) {
                $forceQuote = true;
            }

            /** @var $response Response */
            $html = $this->renderView('BlackSmurfBusinessBundle:Bill:print.html.twig', array(
                'entity' => $entity,
                'benefits' => $benefits,
                'forceQuote' => $forceQuote
                    )
            );

            $nom = $forceQuote ? "devis" : "Facture";
            $nom .= ("_" . $entity->getDateInitial()->format('dmY') . ".pdf");
            $pdf = $this->get("blacksmurfsymfony2core.lib.pdf")->create();
            $pdf->load_html($html);
            $pdf->set_paper("A4", "portrait");
            $pdf->render();
            //$pdf->stream($nom, array("Attachment" => false));

            return new Response($pdf->output(), 200, array('Content-type' => 'application/pdf'));
        } catch (\Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
        } catch (\Exception $e) {
            $this->errorMessage("La facture ne peut pas être supprimé pour la raison suivante : " . $e->getMessage());
        }

        // exit
        return $this->redirect($this->generateUrl($this->getRoute('index')));
    }

    /**
     * Displays a form to accept an existing Bill entity.
     *
     * @Route("/{id}/accept", name="bill_accept")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_GEST, ROLE_CLIENT")
     */
    public function acceptAction(Request $request, $id) {
        try {
            // user can delete this object? => CSRF control
            $this->checkCSRFProtection();

            // get original object from db
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($this->getEntityURI())->find($id);

            $objet = !is_null($this->getMyUserGroupRole()->getCompany()) ? $this->getMyUserGroupRole()->getCompany() : $this->getMyUserGroupRole()->getClient();
            if (is_null($entity) || ($objet != $entity->getCompany() && $objet != $entity->getClient())) {
                $this->errorMessage("Impossible de trouver le devis !");
                return $this->redirect($this->generateUrl($this->getRoute('index')));
            }

            $entity->setAccepted(true);
            $entity->setCharges($entity->getCompany()->getCharges());
            $date = date('Ymd');
            $entity->setDateAccepted(DateTime::createFromFormat('Ymd', $date, new DateTimeZone('UTC')));
            $em->persist($entity);
            $em->flush();
            $this->successMessage("Le devis a bien été validé");
        } catch (\Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
        } catch (\Exception $e) {
            $this->errorMessage("La facture ne peut pas être supprimé pour la raison suivante : " . $e->getMessage());
        }

        // exit
        return $this->redirect($this->generateUrl($this->getRoute('index')));
    }

    /**
     * Show bill's benefit
     *
     * @Route("/benefit/{id}", requirements={"id" = "\d+"}, defaults={"id" = "0"}, name="bill_benefits")
     * @Method("GET")
     * @Template("BlackSmurfBusinessBundle:Bill:list_benefits.html.twig")
     * @Secure(roles="ROLE_CLIENT, ROLE_GEST")
     */
    public function billBenefitAction(Request $request, $id) {
        // user can edit this object? => CSRF control
        try {
            $this->checkCSRFProtection();
        } catch (\Exception $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        // get object from db
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository("BlackSmurfBusinessBundle:Benefit")
                ->findBy(array("bill" => $id));


        // exit
        return $this->buildParameters(array(
                    'entities' => $entities,
                    'bill_id' => $id,
                    'back_url' => $this->getRoute('index')
        ));
    }


    /**
     * Client paid the bill
     *
     * @Route("/{id}/paid", name="bill_paid")
     * @Method("PUT")
     * @Template()
     * @Secure(roles="ROLE_GEST")
     */
    public function paidAction(Request $request, $id) {
        // get original object from db
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->getEntityURI())
            ->findOneBy(array("id" => $id));

        if (is_null($entity) || $entity->getCompany() != $this->getMyUserGroupRole()->getCompany()) {
            $this->errorMessage("Impossible de trouver la facture !");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        // create the form and map it with object
        $editForm = $this->createForm(new BillPaymentType($entity->getId()), $entity, array(
            'action' => $this->generateUrl('bill_paid', array('id' => $entity->getId())),
            'method' => "PUT",
        ));

        // mapping form with request
        $editForm->handleRequest($request);

        // any errors?
        if ($editForm->isValid()) {
            try {

                $em->flush();
                $this->successMessage("Le paiement a bien été renseigné.");

                // exit
                return $this->redirect($this->generateUrl($this->getRoute('index')));
            } catch (\Exception $e) {
                $this->errorMessage("Impossible de réaliser votre demande pour la raison suivante : " . $e->getMessage());
            }
        }

        return $this->buildParameters(array(
            'page_title' => "Mode de règlement de la facture n°" . $entity->getId() . "",
            'entityForm' => $editForm->createView(),
        ));
    }

    /**
     * Client paid the bill
     *
     * @Route("/{id}/paid", name="bill_paid_form")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_GEST")
     */
    public function paidFormAction(Request $request, $id) {
        // user can edit this object? => CSRF control
        try {
            $this->checkCSRFProtection();
        } catch (\Exception $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        // get object from db
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->getEntityURI())
            ->findOneBy(array("id" => $id));

        if (is_null($entity) || $entity->getCompany() != $this->getMyUserGroupRole()->getCompany()) {
            $this->errorMessage("Impossible de trouver la facture !");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        $editForm = $this->createForm(new BillPaymentType($entity->getId()), $entity, array(
            'action' => $this->generateUrl('bill_paid', array('id' => $entity->getId())),
            'method' => "PUT",
        ));

        // exit
        return $this->buildParameters(array(
            'page_title' => "Mode de règlement de la facture n°" . $entity->getId() . "",
            'entityForm' => $editForm->createView(),
        ));
    }


    /**
     * Notify client
     *
     * @Route("/{id}/notify", name="bill_notify")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_GEST")
     */
    public function notifyAction(Request $request, $id) {
        try {
            // user can delete this object? => CSRF control
            $this->checkCSRFProtection();

            // get original object from db
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository($this->getEntityURI())->find($id);

            $objet = !is_null($this->getMyUserGroupRole()->getCompany()) ? $this->getMyUserGroupRole()->getCompany() : $this->getMyUserGroupRole()->getClient();
            if (is_null($entity) || ($objet != $entity->getCompany() && $objet != $entity->getClient())) {
                $this->errorMessage("Impossible de trouver le devis !");
                return $this->redirect($this->generateUrl($this->getRoute('index')));
            }

            $subject="[" . $this->getMyUserGroupRole()->getCompany()->getName() . "] - Un nouveau devis est en ligne" ;
            $from = $this->getMyUserGroupRole()->getCompany()->getEmail();
            $clientId = $entity->getClient()->getId();
            $users = $em->getRepository('BlackSmurfSymfony2CoreBundle:User')->getAllUsersFromClientId($clientId);

            $company = $this->getMyUserGroupRole()->getCompany();
            $content = array();
            $content["devis_name"] = $entity->getTitle();
            $content["mylittlebusiness_uri"] = "http://business.blacksmurf.net";
            $content["name"] = $company->getShortName();
            $content["no_siren"] = $company->getSiren();
            $content["phone_number"] = $company->getPhoneNumber();
            $content["email"] = $company->getEmail();
            foreach ($users as $user) {
                $content["client_name"] = $user->getUsername();
                $this->get('blacksmurfsymfony2core.lib.mailer')->sendNewMessage($from, $user->getLogin(), $subject, $content);
            }

            $this->successMessage("Les clients ont été notifié de la disponibilité du devis " . $entity->getTitle());

        } catch (\Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
        } catch (\Exception $e) {
            $this->errorMessage("Impossible de notifier les clients pour la raison suivante : " . $e->getMessage());
        }

        // exit
        return $this->redirect($this->generateUrl($this->getRoute('index')));

    }

    ////////////////////////////////////////////////////////////////////////////
    // OTHER ///////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    private function getNewId($date, Bill $entity) {
        $benefits = $entity->getBenefits();
        $clientId = $entity->getClient()->getId();

        if ($clientId < 10) {
            $clientId = "00$clientId";
        } else if ($clientId < 100) {
            $clientId = "0$clientId";
        }

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
                "SELECT b.id
                FROM BlackSmurfBusinessBundle:Bill b
                WHERE b.id like '" . $date . $clientId . "%'
                ORDER by b.id DESC"
        );

        $newId = "001";
        $lasts = $query->getResult();
        if (count($lasts) > 0) {
            $lastId = $lasts[0]["id"];
            if (!empty($lastId)) {
                $newId = substr($lastId, 11, 3);
                $newId += 1;

                if ($newId < 10) {
                    $newId = "00$newId";
                } else if ($newId < 100) {
                    $newId = "0$newId";
                }
            }
        }

        return "$date$clientId$newId";
    }

}
