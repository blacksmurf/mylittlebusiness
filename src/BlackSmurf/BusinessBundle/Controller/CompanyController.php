<?php

namespace BlackSmurf\BusinessBundle\Controller;

// SYMFONY //
use Symfony\Component\HttpFoundation\Request;
// ANNOTATIONS //
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
// SYMFONY2COREBUNDLE //
use BlackSmurf\Symfony2CoreBundle\Controller\EntityController;
// PROJECT //
use BlackSmurf\BusinessBundle\Entity\Company;
use BlackSmurf\BusinessBundle\Form\CompanyType;
use BlackSmurf\BusinessBundle\Form\MyCompanyType;

/**
 * Company controller.
 *
 * @Route("/company")
 */
class CompanyController extends EntityController {

    ////////////////////////////////////////////////////////////////////////////
    // MUST BE DEFINED /////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    protected $routes = array(
        "index" => "company_index",
        "new" => "company_new",
        "create" => "company_create",
        "edit" => "company_edit",
        "update" => "company_update",
        "delete" => "company_delete"
    );

    /**
     * Return string object URI
     *
     * @return string
     */
    protected function getEntityURI() {
        return "BlackSmurfBusinessBundle:Company";
    }

    /**
     * Return new object
     *
     * @return Activity
     */
    protected function getNewEntity() {
        return new Company();
    }

    /**
     * Return Form's object
     *
     * @return ActivityType
     */
    protected function getNewEntityFormType() {
        return new CompanyType();
    }

    ////////////////////////////////////////////////////////////////////////////
    // LISTING /////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Lists all Company entities.
     *
     * @Route("/", name="company_index")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_GEST, ROLE_ADMIN")
     */
    public function indexAction() {

        $user = $this->get('security.context');

        if ($user->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();

            $entities = $em->getRepository($this->getEntityURI())
                    ->findAll();

            // render the view
            return $this->buildParameters(array(
                        'entities' => $entities,
            ));
        } else if ($user->isGranted('ROLE_GEST')) {
            $entity = $this->getMyUserGroupRole()->getCompany();

            return $this->render('BlackSmurfBusinessBundle:Company:mycompany.html.twig', array('entity' => $entity));
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // NEW/CREATE //////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Displays a form to create a new Company entity.
     *
     * @Route("/new", name="company_new")
     * @Method("GET")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function newAction() {
        $entity = $this->getNewEntity();

        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST");

        // exit
        return $this->buildParameters(array(
                    'page_title' => 'Ajouter une nouvelle autoentreprise',
                    'entity' => $entity,
                    'entityForm' => $form->createView(),
        ));
    }

    /**
     * Creates a new Company entity.
     *
     * @Route("/", name="company_create")
     * @Method("POST")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function createAction(Request $request) {

        // init the activity object
        $entity = $this->getNewEntity();

        // create the form and map it with object
        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST");

        // mapping form with request
        $form->handleRequest($request);

        // if form is not valid, show the form and exit

        if ($form->isValid()) {
            try {
                // finalize object and persist it to db
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                // exit
                $this->successMessage("L'autoentreprise a bien été ajouté");
                return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getSiret())));
            } catch (\Exception $e) {

                $this->errorMessage("L'autoentreprise ne peut être ajouté");
            }
        }

        // exit
        return $this->buildParameters(array(
                    'page_title' => 'Ajouter une nouvelle autoentreprise',
                    'entity' => $entity,
                    'entityForm' => $form->createView(),
        ));
    }

    ////////////////////////////////////////////////////////////////////////////
    // EDIT/UPDATE /////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Displays a form to edit an existing Company entity.
     *
     * @Route("/{id}/edit", name="company_edit")
     * @Method("GET")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_ADMIN, ROLE_GEST")
     */
    public function editAction($id) {

        // user can edit this object? => CSRF control
        try {
            $this->checkCSRFProtection();
        } catch (\Exception $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        $user = $this->get('security.context');
        if ($user->isGranted('ROLE_GEST')) {
            $id = $this->getMyUserGroupRole()->getCompany()->getSiret();
        }

        // get object from db
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->getEntityURI())
                ->findOneBy(array("siret" => $id));

        if (!is_null($entity)) {
            // create the form and map it with object
            $editForm = $this->createMyForm($entity, $this->generateUrl($this->getRoute('update'), array('id' => $entity->getSiret())), "PUT");

            // pass forms and object to view
            return $this->buildParameters(array(
                        'page_title' => "Modification de la fiche de l'autoentreprise '" . $entity->getName() . "'",
                        'entity' => $entity,
                        'entityForm' => $editForm->createView(),
            ));
        }

        // exit
        $this->errorMessage("Impossible de trouver l'autoentreprise !");
        return $this->redirect($this->generateUrl($this->getRoute('index')));
    }

    /**
     * Edits an existing Company entity.
     *
     * @Route("/{id}", name="company_update")
     * @Method("PUT")
     * @Secure(roles="ROLE_ADMIN, ROLE_GEST")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context');
        $template = "BlackSmurfBusinessBundle:Company:form.html.twig";

        if ($user->isGranted('ROLE_GEST')) {
            $id = $this->getMyUserGroupRole()->getCompany()->getSiret();
            $template = "BlackSmurfBusinessBundle:Company:mycompany.html.twig";
        }

        $entity = $em->getRepository($this->getEntityURI())
                ->findOneBy(array("siret" => $id));

        if (!$entity) {
            $this->errorMessage("Impossible de trouver l'autoentreprise !");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        $editForm = $this->createMyForm($entity, $this->generateUrl($this->getRoute('update'), array('id' => $entity->getSiret())), "PUT");
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            try {

                $file = $request->files->get('blacksmurf_businessbundle_company');
                if (!is_null($file["logoFile"])) {
                    $entity->upload($file["logoFile"]);
                }

                $em->flush();
                $this->successMessage("L'autoentreprise a bien été modifié.");
                return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $id)));
            } catch (\Exception $e) {
                $this->errorMessage("L'autoentreprise ne peut pas être modifié.");
            }
        }

        // pass forms and object to view
        return $this->render($template, $this->buildParameters(array(
                            'page_title' => "Modification de la fiche de l'autoentreprise '" . $entity->getName() . "'",
                            'entity' => $entity,
                            'entityForm' => $editForm->createView()
        )));
    }

    ////////////////////////////////////////////////////////////////////////////
    // DELETE //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes a Company entity.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, defaults={"id" = "0"}, name="company_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id) {
        return $this->deleteMyEntityFromId($request, $id);
    }

}
