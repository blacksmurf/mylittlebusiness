<?php

namespace BlackSmurf\BusinessBundle\Controller;

// SYMFONY //
use Symfony\Component\HttpFoundation\Request;
// ANNOTATIONS //
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
// SYMFONY2COREBUNDLE //
use BlackSmurf\Symfony2CoreBundle\Controller\EntityController;
// PROJECT //
use BlackSmurf\BusinessBundle\Entity\Client;
use BlackSmurf\BusinessBundle\Form\ClientType;

/**
 * Client controller.
 *
 * @Route("/client")
 */
class ClientController extends EntityController {

    ////////////////////////////////////////////////////////////////////////////
    // MUST BE DEFINED /////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    protected $routes = array(
        "index" => "client_index",
        "new" => "client_new",
        "create" => "client_create",
        "edit" => "client_edit",
        "update" => "client_update",
        "delete" => "client_delete"
    );

    /**
     * Return string object URI
     *
     * @return string
     */
    protected function getEntityURI() {
        return "BlackSmurfBusinessBundle:Client";
    }

    /**
     * Return new object
     *
     * @return Client
     */
    protected function getNewEntity() {
        return new Client();
    }

    /**
     * Return Form's object
     *
     * @return ClientType
     */
    protected function getNewEntityFormType() {
        return new ClientType();
    }

    ////////////////////////////////////////////////////////////////////////////
    // LISTING /////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Lists all Client entities.
     *
     * @Route("/", name="client_index")
     * @Method("GET")
     * @Template()
     * @Secure(roles="ROLE_GEST")
     */
    public function indexAction() {

        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        $entities = NULL;

        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
        } else {

            // select all activities from user's profile (company)
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository($this->getEntityURI())
                ->findBy(array("company" => $userGroupRole->getCompany()));
        }

        // render the view
        return $this->buildParameters(array(
                    'entities' => $entities,
        ));
    }

    ////////////////////////////////////////////////////////////////////////////
    // NEW/CREATE //////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Displays a form to create a new Client entity.
     *
     * @Route("/new", name="client_new")
     * @Method("GET")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function newAction() {

        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
            return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
        }

        // init the activity object
        $entity = $this->getNewEntity();

        // set default benefit's price of the company
        $entity->setPercent(0);

        // create the object's form
        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST");

        // render the view
        return $this->buildParameters(array(
                    'page_title' => 'Ajouter un client',
                    'entity' => $entity,
                    'entityForm' => $form->createView(),
        ));
    }

    /**
     * Creates a new Client entity.
     *
     * @Route("/", name="client_create")
     * @Method("POST")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function createAction(Request $request) {

        // if user haven't a company
        $userGroupRole = $this->getMyUserGroupRole();
        if (is_null($userGroupRole->getCompany())) {
            $this->errorMessage("Vous n'avez aucune référence d'autoentreprise dans votre profil !");
            return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
        }

        // init the activity object
        $entity = $this->getNewEntity();

        // create the form and map it with object
        $form = $this->createMyForm($entity, $this->generateUrl($this->getRoute('create')), "POST");

        // mapping form with request
        $form->handleRequest($request);

        // if form is not valid, show the form and exit
        if ($form->isValid()) {
            try {
                // finalize object and persist it to db
                $entity->setCompany($userGroupRole->getCompany());

                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                // exist
                $this->successMessage("Le client a bien été ajouté.");
                return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $entity->getId())));
            } catch (\Exception $e) {

                $this->errorMessage("Le client ne peut être ajouté.");
            }
        }

        // exit
        return $this->buildParameters(array(
                    'page_title' => 'Ajouter un client',
                    'entity' => $entity,
                    'entityForm' => $form->createView(),
        ));
    }

    ////////////////////////////////////////////////////////////////////////////
    // EDIT/UPDATE /////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * Displays a form to edit an existing Client entity.
     *
     * @Route("/{id}/edit", name="client_edit")
     * @Method("GET")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function editAction($id) {

        // user can edit this object? => CSRF control
        try {
            $this->checkCSRFProtection();
        } catch (\Exception $e) {
            $this->errorMessage("Impossible de réaliser votre demande");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        // get object from db
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->getEntityURI())
                ->findOneBy(array("id" => $id));

        if (!is_null($entity)) {
            // create the form and map it with object
            $editForm = $this->createMyForm($entity, $this->generateUrl($this->getRoute('update'), array('id' => $entity->getId())), "PUT");

            // pass forms, object to view and exit
            return $this->buildParameters(array(
                        'page_title' => "Modification de la fiche client '" . $entity->getName() . "'",
                        'entity' => $entity,
                        'entityForm' => $editForm->createView(),
            ));
        }

        $this->errorMessage("Impossible de trouver le client !");
        return $this->redirect($this->generateUrl($this->getRoute('index')));
    }

    /**
     * Edits an existing Client entity.
     *
     * @Route("/{id}", name="client_update")
     * @Method("PUT")
     * @Template("BlackSmurfSymfony2CoreBundle:Crud:form.html.twig")
     * @Secure(roles="ROLE_GEST")
     */
    public function updateAction(Request $request, $id) {

        // get original object from db
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository($this->getEntityURI())
                ->findOneBy(array("id" => $id));

        if (is_null($entity)) {
            $this->errorMessage("Impossible de trouver le client !");
            return $this->redirect($this->generateUrl($this->getRoute('index')));
        }

        // create the form and map it with object
        $editForm = $this->createMyForm($entity, $this->generateUrl($this->getRoute('update'), array('id' => $entity->getId())), "PUT");

        // mapping form with request
        $editForm->handleRequest($request);

        // any errors?
        if ($editForm->isValid()) {
            try {
                // finalize object and persist it to db
                $em->flush();
                $this->successMessage("Le client a bien été modifié.");

                // exist
                return $this->redirect($this->generateUrl($this->getRoute('index'), array('id' => $id)));
            } catch (\Exception $e) {

                $this->errorMessage("Le client ne peut pas être modifié.");
            }
        }

        // exit
        return $this->buildParameters(array(
                    'page_title' => "Modification de la fiche client '" . $entity->getName() . "'",
                    'entity' => $entity,
                    'entityForm' => $editForm->createView(),
        ));
    }

    ////////////////////////////////////////////////////////////////////////////
    // DELETE //////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Deletes a Client entity.
     *
     * @Route("/{id}", requirements={"id" = "\d+"}, defaults={"id" = "0"}, name="client_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_GEST")
     */
    public function deleteAction(Request $request, $id) {
        return $this->deleteMyEntityFromId($request, $id);
    }

}
