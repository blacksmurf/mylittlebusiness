<?php

namespace BlackSmurf\BusinessBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', array('label' => 'Identité :', 'max_length' => 255))
                ->add('address', 'textarea', array('label' => 'Adresse postale :', 'max_length' => 255))
                ->add('phoneNumber', 'text', array('label' => 'Téléphone :', 'max_length' => 10))
                ->add('email', 'text', array('label' => 'E-mail : ', 'max_length' => 100))
                ->add('percent', 'number', array('label' => 'Surcoût tarifaire (en %) :', 'max_length' => 255, 'attr' => array('style' => 'width: 50px')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BlackSmurf\BusinessBundle\Entity\Client',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'blacksmurf_businessbundle_client';
    }

}
