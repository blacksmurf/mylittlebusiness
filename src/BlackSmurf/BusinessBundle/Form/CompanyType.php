<?php

namespace BlackSmurf\BusinessBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;


class CompanyType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $company = $this;

        $builder
                ->add('name', 'text', array('label' => 'Nom :', 'max_length' => 255))
                ->add('shortName', 'text', array('label' => 'Nom court :', 'max_length' => 255))
                ->add('address', 'textarea', array('label' => 'Adresse :', 'max_length' => 255, 'attr' => array('style' => 'width: 400px')))
                ->add('email', 'text', array('label' => 'Email :', 'max_length' => 50, 'attr' => array('style' => 'width: 400px')))
                ->add('phoneNumber', 'text', array('label' => 'Téléphone :', 'max_length' => 10, 'attr' => array('style' => 'width: 120px')))
                ->add('siret', 'text', array('label' => 'Code SIRET :', 'max_length' => 14, 'attr' => array('style' => 'width: 140px')))
                ->add('apeCode', 'text', array('label' => 'Code APE :', 'max_length' => 5, 'attr' => array('style' => 'width: 140px')))
                ->add('startDateActivity', 'date', array('widget' => 'single_text', 'label' => 'Date de début d\'activité :', 'max_length' => 255))
                ->add('eirlStatus', 'checkbox', array('label' => 'EIRL :', 'max_length' => 255, 'required' => false))
                ->add('defaultPrice', 'number', array('label' => 'Coût unitaire :', 'max_length' => 5, 'attr' => array('style' => 'width: 80px')))
                ->add('logoFile', 'file', array('mapped' => false, 'data_class' => null, 'label' => 'Logo :', 'required' => false))
                ->add('charges', 'number', array('label' => 'Charges (%):', 'max_length' => 5, 'attr' => array('style' => 'width: 80px')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BlackSmurf\BusinessBundle\Entity\Company',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'blacksmurf_businessbundle_company';
    }

}
