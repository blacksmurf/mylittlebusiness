<?php

namespace BlackSmurf\BusinessBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class BillType extends AbstractType {

    private $company;
    private $isNew;
    private $billId;

    public function __construct($company, $new, $billId) {
        $this->company = $company;
        $this->isNew = $new;
        $this->billId = $billId;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $company = $this->company;

        $queryAnd = "1 = 1";
        if ($this->isNew) {
            $queryAnd = "b.bill is null";
        } else {
            $queryAnd = "(b.bill = " . $this->billId . " or b.bill is null)";
        }

        $builder
                ->add('title', 'text', array('label' => 'Nom de la facture :', 'max_length' => 255))
                ->add('totalPrice', 'number', array('label' => 'Montant total :', 'max_length' => 255, 'read_only' => true, 'attr' => array('style' => 'width: 80px')))
                ->add('handOff', 'number', array('label' => 'Remise (en %) :', 'max_length' => 255, 'required' => false, 'attr' => array('style' => 'width: 50px')))
                ->add('accepted', 'checkbox', array('label' => 'Devis accepté :', 'required' => false))
                ->add('client', 'entity', array(
                    'class' => 'BlackSmurf\BusinessBundle\Entity\Client',
                    'property' => 'name',
                    'empty_value' => null,
                    'query_builder' => function(EntityRepository $er) use ($company) {
                        return $er->createQueryBuilder('u')
                                ->where('u.company = :identifier')
                                ->setParameter('identifier', $company);
                    },
                    'label' => 'Client :'))
                ->add('benefits', 'entity', array(
                    'label' => 'Mes prestations',
                    'class' => 'BlackSmurf\BusinessBundle\Entity\Benefit',
                    'query_builder' => function(EntityRepository $er ) use ( $company, $queryAnd ) {
                        return $er->createQueryBuilder('b')
                                ->join('b.client', 'c')
                                ->where('c.company = :identifier')
                                ->andWhere($queryAnd)
                                ->setParameter('identifier', $company);
                    },
                    'required' => true,
                    'multiple' => true,
                    'expanded' => true))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BlackSmurf\BusinessBundle\Entity\Bill',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'blacksmurf_businessbundle_bill';
    }

}
