<?php

namespace BlackSmurf\BusinessBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;

class BillPaymentType extends AbstractType {

    private $company;
    private $billId;

    public function __construct($billId) {
        $this->billId = $billId;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {


        $builder
            ->add('payment_mode', 'choice', array(
                'choice_list' => new ChoiceList(
                    array(1, 2, 3, 4),
                    array("Chèque", "Carte bancaire", "Liquide", "Paypal")
                ),
                'label' => "Mode de règlement", 'required' => true
            ))
            ->add('payment_reference', 'text', array('label' => 'Référence :', 'max_length' => 64, 'required' => true))
            ->add('payment_date', 'date', array('widget' => 'single_text', 'label' => 'Date :'))
        ;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'blacksmurf_businessbundle_bill';
    }

}
