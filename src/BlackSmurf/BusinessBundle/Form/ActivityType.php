<?php

namespace BlackSmurf\BusinessBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActivityType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', array('label' => 'Nom :', 'max_length' => 255))
                ->add('description', 'text', array('label' => 'Description :', 'max_length' => 255))
                ->add('price', 'number', array('label' => 'Coût unitaire :', 'max_length' => 5, 'attr' => array('style' => 'width: 80px')))
        ;
//                ->add('company', 'entity', array('label' => "Autoentreprise : ", 'placeholder' => '', 'class' => 'BlackSmurf\BusinessBundle\Entity\Company', 'property' => 'siret'))
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BlackSmurf\BusinessBundle\Entity\Activity',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'blacksmurf_businessbundle_activity';
    }

}
