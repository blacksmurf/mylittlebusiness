<?php

namespace BlackSmurf\BusinessBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class BenefitType extends AbstractType {

    private $company;

    public function __construct($company) {
        $this->company = $company;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        /*
          'query_builder' => function(EntityRepository $er) {
          return $er->createQueryBuilder('u')
          ->orderBy('u.username', 'ASC');
          },
         */

        $company = $this->company;

        $builder
                ->add('client', 'entity', array(
                    'class' => 'BlackSmurf\BusinessBundle\Entity\Client',
                    'property' => 'name',
                    'empty_value' => null,
                    'query_builder' => function(EntityRepository $er) use ($company) {
                        return $er->createQueryBuilder('u')
                                ->where('u.company = :identifier')
                                ->setParameter('identifier', $company);
                    },
                    'label' => 'Client :'))
                ->add('activity', 'entity', array(
                    'class' => 'BlackSmurf\BusinessBundle\Entity\Activity',
                    'property' => 'completeName',
                    'query_builder' => function(EntityRepository $er) use ($company) {
                        return $er->createQueryBuilder('u')
                                ->where('u.company = :identifier')
                                ->setParameter('identifier', $company);
                    },
                    'label' => 'Activité :'))
                ->add('date', 'date', array('widget' => 'single_text', 'label' => 'Date :'))
                ->add('quantity', 'number', array('label' => 'Quantité :', 'max_length' => 5, 'attr' => array('style' => 'width: 80px')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BlackSmurf\BusinessBundle\Entity\Benefit',
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'blacksmurf_businessbundle_benefit';
    }

}
