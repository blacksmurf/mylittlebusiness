<?php

namespace BlackSmurf\BusinessBundle\DataFixtures\ORM;

use \DateTime;
use \DateTimeZone;
use \Symfony\Component\DependencyInjection\ContainerAwareInterface;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use \Doctrine\Common\Persistence\ObjectManager;
use \BlackSmurf\BusinessBundle\Entity\Company;
use \BlackSmurf\BusinessBundle\Entity\Activity;
use \BlackSmurf\BusinessBundle\Entity\Client;
use \BlackSmurf\BusinessBundle\Entity\Charge;
use \BlackSmurf\Symfony2CoreBundle\Entity\User;
use \BlackSmurf\Symfony2CoreBundle\Entity\UserGroupRole;
use \BlackSmurf\Symfony2CoreBundle\Entity\GroupRole;

class LoadBusinessModelData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {

    private $container;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    /**
     *
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager) {
        $company = new Company();
        $company->setSiret("99999999999999");
        $company->setName("MON AUTOENTREPRISE");
        $company->setShortName("John Doe");
        $company->setApeCode("6202A");
        $date = DateTime::createFromFormat('d-m-Y', '01-05-2014', new DateTimeZone('UTC'));
        $company->setStartDateActivity($date);
        $company->setDefaultPrice(40);
        $company->setEmail("contact@entrepreneur.fr");
        $company->setPhoneNumber("0699999999");
        $company->setAddress("Mon adresse - Ville");
        $company->setEirlStatus(true);

        // Charges sociales + Charges fiscales  + Formation professionnelle + Frais de Chambre consulaire
        // 22,9% + 1,7% + 0,3% + 0,48%
        $company->setCharges(25.38);

        $client = new Client();
        $client->setName("Cabinet d'architecture");
        $client->setAddress("Adresse du cabinet - Ville");
        $client->setCompany($company);
        $client->setEmail("cabinet@test.fr");
        $client->setPhoneNumber("0442208889");
        $client->setPercent(0);

        $activity1 = new Activity();
        $activity1->setId("MI001");
        $activity1->setName("MAINTENANCE INFORMATIQUE");
        $activity1->setDescription("conseil, expertise sur de la maintenance de parc informatique");
        $activity1->setCompany($company);
        $activity1->setPrice($company->getDefaultPrice());

        $em = $this->container->get('doctrine')->getEntityManager('default');

        $user1 = new User();
        $user1->setLogin("entrepreneur@entrepreneur.fr");
        $user1->setUsername("Entrepreneur");
        $user1->setPassword("test");

        $user2 = new User();
        $user2->setLogin("client@client.fr");
        $user2->setUsername("Client");
        $user2->setPassword("test");

        $groupRole1 = new GroupRole();
        $groupRole1->setName("Entrepreneur");
        $groupRole1->setRole("ROLE_GEST");

        $groupRole2 = new GroupRole();
        $groupRole2->setName("Client");
        $groupRole2->setRole("ROLE_CLIENT");

        $userGroupRole1 = new UserGroupRole();
        $userGroupRole1->setUser($user1);
        $userGroupRole1->setGroupRole($groupRole1);
        $userGroupRole1->setCompany($company);

        $userGroupRole2 = new UserGroupRole();
        $userGroupRole2->setUser($user2);
        $userGroupRole2->setGroupRole($groupRole2);
        $userGroupRole2->setClient($client);

        $userGroupRole3 = new UserGroupRole();
        $userGroupRole3->setUser($user1);
        $userGroupRole3->setGroupRole($groupRole2);
        $userGroupRole3->setClient($client);

        $manager->persist($company);
        $manager->persist($client);
        $manager->persist($activity1);
        $manager->flush();

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($groupRole1);
        $manager->persist($groupRole2);
        $manager->persist($userGroupRole1);
        $manager->persist($userGroupRole2);
        $manager->persist($userGroupRole3);
        $manager->flush();
    }

    /**
     *
     * {@inheritDoc}
     */
    public function getOrder() {
        return 2;
    }

}
