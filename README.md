
=== Procédure d'installation ===
 php app/console doctrine:generate:entities Smurf
 php app/console doctrine:schema:update --force
 php app/console doctrine:fixtures:load
 php app/console assets:install
 php app/console cache:clear
 service apache2 reload
